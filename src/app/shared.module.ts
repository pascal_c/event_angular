/**
 *
 * (c) 2013-2017 Wishtack
 *
 * $Id: $
 */

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    imports: SharedModule.MODULES,
    exports: SharedModule.MODULES
})
export class SharedModule {

    static MODULES = [
        BrowserModule,
        FormsModule,
        HttpModule
    ];

}

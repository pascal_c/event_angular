/**
 *
 * (c) 2013-2017 Wishtack
 *
 * $Id: $
 */

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import { WtEvent } from '../wt-event';
import { EventStore } from '../event-store';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'wt-event-list-container',
  templateUrl: './event-list-container.component.html'
})
export class EventListContainerComponent implements OnInit {

  obsEventList: Observable<WtEvent[]>;
  selectedEvent: WtEvent;

  constructor(private _eventStore: EventStore) {
    this.obsEventList = this._eventStore.obsEventList;
  }

  ngOnInit() {
    this._eventStore.getEventList().subscribe();
  }


  addEvent(event: WtEvent) {
    this._eventStore.addEvent(event).subscribe();
  }

  selectEvent(event: WtEvent) {
    this.selectedEvent = event;
  }

  removeEvent(event: WtEvent) {
    this._eventStore.removeEvent(event.idEvent).subscribe();
  }

}

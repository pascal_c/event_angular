/**
 *
 * (c) 2013-2017 Wishtack
 *
 * $Id: $
 */

export class WtEvent {

  public idEvent?: string;
  public title?: string;
  public description?: string;
//  public isRemoveInProgress? = false;

  static fromObject(data) {
    /* Think about using named parameters. */
    return new WtEvent(data);
  }

  // constructor({idEvent, title, description, isRemoveInProgress}: WtEvent = {}) {
  //   this.idEvent = idEvent;
  //   this.title = title;
  //   this.description = description;
  //   this.isRemoveInProgress = isRemoveInProgress;
  // }
  constructor({idEvent, title, description}: WtEvent = {}) {
    this.idEvent = idEvent;
    this.title = title;
    this.description = description;
  }
}

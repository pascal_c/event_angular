import { WtEvent } from './wt-event';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, ReplaySubject } from 'rxjs';
/**
 *
 * (c) 2013-2017 Wishtack
 *
 * $Id: $
 */

@Injectable()
export class EventStore {

  // private _apiBaseUrl = 'http://wishtack-events.getsandbox.com';
  // private _apiEventUrl: string;
  private _eventList: WtEvent[];
  private _eventListSubject = new ReplaySubject<WtEvent[]>(1);

  constructor(private _http: Http) {
  }

  get obsEventList(): Observable<WtEvent[]> {
    return this._eventListSubject.asObservable();
  }

  addEvent(event: WtEvent): Observable<WtEvent> {
    return this._http.post('http://localhost:8080/event/api/event2/creation/1', event)
      .map((response) => WtEvent.fromObject(response.json()))
      .do((event) => {
        this._updateEventList([...this._eventList, event]);
      });
  }

  removeEvent(eventId: string) {

    // this._updateEventList(this._eventList.map((event) => {
    //
    //   if (event.idEvent === eventId) {
    //     event = new WtEvent(event);
    //     event.isRemoveInProgress = true;
    //   }
    //
    //   return event;
    // }));

    return this._http.delete(`http://localhost:8080/event/api/event2/delete/${eventId}`)
      .do(() => {
        let eventList = this._eventList
          .filter((event) => event.idEvent !== eventId);
        this._updateEventList(eventList);
      });
  }

  getEventList(): Observable<WtEvent[]> {
    return this._http.get('http://localhost:8080/event/api/event2/list')
      .map((response) => response.json()
        .map((data) => WtEvent.fromObject(data)))
      .do((eventList) => this._updateEventList(eventList));
  }

  private _updateEventList(eventList: WtEvent[]) {
    this._eventList = eventList;
    this._eventListSubject.next(this._eventList);
  }

}

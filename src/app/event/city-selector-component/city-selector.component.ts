import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'wt-city-selector',
    templateUrl: './city-selector.component.html',
    styleUrls: ['./city-selector.component.css']
})
export class CitySelectorComponent {

    @Output() onCitySelect = new EventEmitter();

    cityList = ['Nice', 'Antibes'];

    selectCity(city) {
        this.onCitySelect.emit(city);
    }

}

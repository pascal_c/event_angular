/**
 *
 * (c) 2013-2017 Wishtack
 *
 * $Id: $
 */

import { EventListContainerComponent } from './event-list-container-component/event-list-container.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared.module';
import { EventFormComponent } from './event-form-component/event-form.component';
import { EventListComponent } from './event-list-component/event-list.component';
import { EventStore } from './event-store';

@NgModule({
    declarations: EventModule.COMPONENTS,
    exports: EventModule.COMPONENTS,
    imports: [
        SharedModule
    ],
    providers: [
        EventStore
    ]
})
export class EventModule {

    static COMPONENTS = [
        EventFormComponent,
        EventListComponent,
        EventListContainerComponent
    ];

}

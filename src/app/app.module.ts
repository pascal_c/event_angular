import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EventModule } from './event/event.module';
import { SharedModule } from './shared.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        EventModule,
        SharedModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
